<?php
/*
Plugin Name: DNC's Simple Twitter Widget
Plugin URI: http://www.andadv.com/
Description: Displays a public Twitter feed
Author: David Corona & Kyle Jordan
Version: 1.1
Author URI: http://www.andadv.com/
*/

require_once('twitterAPIExchange.php');

// Display Twitter messages
function dnc_simple_twitter_messages($options)
{

    if ($options['username'] == '') {
        return 'Twitter username not configured';
    }
    if ($options['oauth_access_token'] == '') {
        return 'Access token not configured';
    }
    if ($options['oauth_access_token_secret'] == '') {
        return 'Access token secret not configured';
    }
    if ($options['consumer_key'] == '') {
        return 'Consumer key not configured';
    }
    if ($options['consumer_secret'] == '') {
        return 'Consumer secret not configured';
    }
    if (!is_numeric($options['num']) or $options['num'] <= 0) {
        return 'Number of tweets not valid';
    }
    $max_items_to_retrieve = $options['num'];

    $settings = array(
        'oauth_access_token' => $options['oauth_access_token'],
        'oauth_access_token_secret' => $options['oauth_access_token_secret'],
        'consumer_key' => $options['consumer_key'],
        'consumer_secret' => $options['consumer_secret']
    );

    $twitter = new TwitterAPIExchange($settings);
    $requestMethod = 'GET';

    // USE TRANSIENT DATA, TO MINIMIZE REQUESTS TO THE TWITTER FEED
    $timeout = 30 * 60; //30m
    $error_timeout = 5 * 60; //5m
    $no_cache_timeout = 60 * 60 * 24 * 365 * 10; //10 years should be fine...

    $transient_name = 'twitter_data_' . $options['username'] . '_' . $options['num'];

    $twitter_data = get_transient($transient_name);
    $twitter_status = get_transient($transient_name . "_status");

    // Twitter Status
    if (!$twitter_status || !$twitter_data) {
        $url = 'https://api.twitter.com/1/account/rate_limit_status.json';
        $postfields = array(
            'resources' => 'statuses'
        );
        $getfield = '?' . TwitterAPIExchange::buildQueryString($postfields);

        $json = $twitter->setGetfield($getfield)
                        ->buildOauth($url, $requestMethod)
                        ->performRequest();

        $twitter_status = json_decode($json, true);

        set_transient($transient_name . "_status", $twitter_status, $no_cache_timeout);
    }
    $reset_seconds = (strtotime($twitter_status['reset_time']) - time());


    // Tweets
    if (!$twitter_data) {
        if ($twitter_status['resources']['statuses']['/statuses/user_timeline']['remaining'] <= 7) {
            $timeout = $reset_seconds;
            $error_timeout = $timeout;
        }

        $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
        $postfields = array(
            'screen_name' => $options['username'],
            'count' => $max_items_to_retrieve
        );
        $getfield = '?' . TwitterAPIExchange::buildQueryString($postfields);

        $json = $twitter->setGetfield($getfield)
                        ->buildOauth($url, $requestMethod)
                        ->performRequest();

        $twitter_data = json_decode($json, true);

        if (isset($twitter_data['errors'])) $twitter_data['error'] = $twitter_data['errors']['message'];

        if (!isset($twitter_data['error']) && (count($twitter_data) == $options['num'])) {
            set_transient($transient_name, $twitter_data, $timeout);
            set_transient($transient_name . "_valid", $twitter_data, $no_cache_timeout);
        } else {
            set_transient($transient_name, $twitter_data, $error_timeout); // Wait 5 minutes before retry
            if (!isset($twitter_data['error'])) $twitter_data['error'] = "";
            echo "\n<!-- Twitter error: " . $twitter_data['error'] . " -->";
        }
    } else {
        echo "\n<!-- Twitter error: No Data -->";
    }

    $items_retrieved = count($twitter_data);

    if (empty($twitter_data) || isset($twitter_data['error'])) {
        if (false === ($twitter_data = get_transient($transient_name . "_valid"))) {
            return 'No public tweets';
        }
    }

    // SET THE MAX NUMBER OF ITEMS
    $num_items_shown = $options['num'];
    if ($items_retrieved < $options['num']) {
        $num_items_shown = $items_retrieved;
    }

    $out = '<ul class="really_simple_twitter_widget ' . ( $options['ulclass'] ? $options['ulclass'] : '' ) . '">';

    $i = 0;
    foreach ($twitter_data as $message) {
        if ($i >= $num_items_shown) {
            break;
        }
        $msg = wp_html_excerpt($message['text'], $options['excerpt_length']);

        $out .= '<li' . ( $options['liclass'] ? ' class="' . $options['liclass'] . '"' : '' ) . '>';

        if($options['link_tweet']) {
            $out .= '<a href="http://twitter.com/' . $options['username'] . '/status/' . $message['id_str'] . '" target="_blank">';
            if(!$options['hide_date']) $out .= '<span class="date">' . date_i18n('M. d', strtotime($message['created_at'])) . '</span> - ';
            $out .= $msg;
            $out .= '</a>';
        }
        else {
            if(!$options['hide_date']) $out .= '<span class="date">' . date_i18n('M. d', strtotime($message['created_at'])) . '</span> - ';
            $out .= preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', $msg);
            $out .= '<a class="more_arrow" href="http://twitter.com/' . $options['username'] . '/status/' . $message['id_str'] . '" target="_blank"></a>';
        }

        $out .= '</li>';
        $i++;
    }
    $out .= '</ul>';
    if(!$options['hide_more']) $out .= '<a class="more" href="http://twitter.com/#!/' . $options['username'] . '" target="_blank">MORE</a>';

    return $out;
}


/**
 * DNCSimpleTwitterWidget Class
 */
class DNCSimpleTwitterWidget extends WP_Widget
{
    private /** @type {string} */
        $languagePath;

    /** constructor */
    function DNCSimpleTwitterWidget()
    {
        $this->options = array(
            array(
                'name' => 'title',
                'label' => 'Title',
                'type' => 'text'
            ),
            array(
                'name' => 'username',
                'label' => 'Twitter Username',
                'type' => 'text'
            ),
            array(
                'name' => 'num',
                'label' => 'Show # of Tweets',
                'type' => 'text'
            ),
            array(
                'name' => 'excerpt_length',
                'label' => 'Excerpt Length',
                'type' => 'text'
            ),
            array(
                'name' => 'oauth_access_token',
                'label' => 'Access token',
                'type' => 'text'
            ),
            array(
                'name' => 'oauth_access_token_secret',
                'label' => 'Access token secret',
                'type' => 'text'
            ),
            array(
                'name' => 'consumer_key',
                'label' => 'Consumer key',
                'type' => 'text'
            ),
            array(
                'name' => 'consumer_secret',
                'label' => 'Consumer secret',
                'type' => 'text'
            )
        );
        $widget_ops = array('description' => 'Displays a public Twitter feed');
        parent::WP_Widget(false, $name = 'DNC Simple Twitter Widget', $widget_ops);
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance)
    {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        echo $before_widget;
        if ($title) {
            echo $before_title . $instance['title'] . $after_title;
        }
        echo dnc_simple_twitter_messages($instance);
        echo $after_widget;
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;

        foreach ($this->options as $val) {
            if ($val['type'] == 'text') {
                $instance[$val['name']] = strip_tags($new_instance[$val['name']]);
            } else if ($val['type'] == 'checkbox') {
                $instance[$val['name']] = ($new_instance[$val['name']] == 'on') ? true : false;
            }
        }
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance)
    {
        if (empty($instance)) {
            $instance['title'] = 'Lastest Tweets';
            $instance['username'] = '';
            $instance['num'] = '5';
            $instance['excerpt_length'] = '160';
            $instance['oauth_access_token'] = '';
            $instance['oauth_access_token_secret'] = '';
            $instance['consumer_key'] = '';
            $instance['consumer_secret'] = '';
        }

        foreach ($this->options as $val) {
            $label = '<label for="' . $this->get_field_id($val['name']) . '">' . $val['label'] . '</label>';
            if ($val['type'] == 'text') {
                echo '<p>' . $label . '<br />';
                echo '<input class="widefat" id="' . $this->get_field_id($val['name']) . '" name="' . $this->get_field_name($val['name']) . '" type="text" value="' . esc_attr($instance[$val['name']]) . '" /></p>';
            } else if ($val['type'] == 'checkbox') {
                $checked = ($instance[$val['name']]) ? 'checked="checked"' : '';
                echo '<input id="' . $this->get_field_id($val['name']) . '" name="' . $this->get_field_name($val['name']) . '" type="checkbox" ' . $checked . ' /> ' . $label . '<br />';
            }
        }
    }
} // class DNCSimpleTwitterWidget

// register DNCSimpleTwitterWidget widget
add_action('widgets_init', create_function('', 'return register_widget("DNCSimpleTwitterWidget");'));

// Add DNC Simple Twitter Shortcode
add_shortcode( 'dnc_simple_twitter', 'dnc_simple_twitter_messages' );

