<?php
/*
Plugin Name: AMG's DevFriendly Social Feeds
Plugin URI: http://www.andadv.com/
Description: Displays social feeds in a lightweight, developer friendly, easy to style fashion. Developed so you spend less time setting up and more time working on more complex things.
Author: David Corona, Kyle Jordan & Matt Bengston
Version: 0.5
Author URI: http://www.andadv.com/
License: GPL3

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  Copyright (C) 2014  Anderson Marketing Group  (email : mbengston@andersonmarketing.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/

//**// Define constants
define ('AMGROOT', plugin_dir_path(__FILE__));
define ('AMGURL', plugins_url(NULL, __FILE__));

//**// Grab API's
require_once AMGROOT.'/api/facebook.php';
require_once AMGROOT.'/api/twitterAPIExchange.php';

class AMGDevFriendlySocialFeeds extends WP_Widget
{

}