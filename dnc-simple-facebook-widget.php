<?php
/*
Plugin Name: DNC's Simple Facebook Widget
Plugin URI: http://www.andadv.com/
Description: Displays public facebook posts
Author: David Corona
Version: 1.0
Author URI: http://www.andadv.com/
*/


class DNCSimpleFacebookWidget extends WP_Widget {
    function DNCSimpleFacebookWidget() {
        $widget_ops = array('classname' => 'widget_facebook', 'description' => 'Displays a facebook feed');
        $this->WP_Widget('widget_dnc_facebook', 'DNC Simple Facebook Widget', $widget_ops);
        $this->alt_option_name = 'widget_dnc_facebook';

        add_action('save_post', array(&$this, 'flush_widget_cache'));
        add_action('deleted_post', array(&$this, 'flush_widget_cache'));
        add_action('switch_theme', array(&$this, 'flush_widget_cache'));
    }

    function widget($args, $instance) {
        $cache = wp_cache_get('widget_dnc_facebook', 'widget');

        if (!is_array($cache)) {
            $cache = array();
        }
        if (!isset($args['widget_id'])) {
            $args['widget_id'] = null;
        }
        if (isset($cache[$args['widget_id']])) {
            echo $cache[$args['widget_id']];
            return;
        }

        ob_start();
        extract($args, EXTR_SKIP);

        $title = apply_filters('widget_title', empty($instance['title']) ? 'Facebook Feed' : $instance['title'], $instance, $this->id_base);
        if (!isset($instance['facebook_link'])) { return; }
        $fb_id = $instance['facebook_link'];

        echo $before_widget;
        if ($title) {
//            echo '<img class="fb_icon" src="' . get_template_directory_uri() . '/img/fb_icon.jpg" width="21" height="21" />';
            echo $before_title;
            echo $title;
            echo $after_title; ?>
        <?php }

        $fb_res = wp_remote_get("https://graph.facebook.com/" . $fb_id . "/posts?access_token=386655328981|7StQjnmxME5Qr7w0DUpg4LXLqLg&limit=" . $instance['max_results']);
        if( is_wp_error( $fb_res ) ) {
            return 'Error retrieving facebook feed';
        } else {
            $fb_json = json_decode($fb_res['body'], true);
        }

        ?>
    <ul>
        <?php
        for ($x = 0; $x < count($fb_json['data']); $x++) {
            isset($fb_json['data'][$x]['description']) ? $fb_message = $fb_json['data'][$x]['description'] : $fb_message = $fb_json['data'][$x]['message'];
            if (isset($fb_json['data'][$x]['link'])) {
                $fb_link = $fb_json['data'][$x]['link'];
            } else {
                $story_id = explode('_', $fb_json['data'][$x]['id']);
                $fb_link = 'https://www.facebook.com/permalink.php?story_fbid=' . $story_id[1] . '&id=' . $fb_id;
            }
            if (strlen($fb_message) > intval($instance['excerpt_length'])) {
                $fb_message = substr($fb_message, 0, intval($instance['excerpt_length'])) . "[...]";
            }

            echo "<li>";
            echo '<span class="date">' . date_i18n('M. d', strtotime($fb_json['data'][$x]['created_time'])) . '</span> - ';
            echo $fb_message;
//            echo '<a href="' . $fb_link . '" target="_blank">';
//            if (isset($fb_json['data'][$x]['picture'])) {
//                $fb_image = substr($fb_json['data'][$x]['picture'], strpos($fb_json['data'][$x]['picture'], 'url=') + 4);
//                echo '<img src="' . urldecode($fb_image) . '" width="40" height="40" />';
//            } else {
//                echo '<img src="https://graph.facebook.com/' . $fb_id . '/picture?access_token=386655328981|7StQjnmxME5Qr7w0DUpg4LXLqLg" width="40" height="40" />';
//            }
//            echo "<div>";
//            if (isset($fb_json['data'][$x]['name'])) echo $fb_json['data'][$x]['name'] . "<br />";
//            if (isset($fb_json['data'][$x]['to'])) {
//                echo $fb_json['data'][$x]['to']['data'][0]['name'];
//            } else if (isset($fb_json['data'][$x]['from'])) {
//                echo $fb_json['data'][$x]['from']['name'];
//            }
//            echo "</div></a>";
//            if ($x < (count($fb_json['data']) - 1)) echo "<hr />";
            echo '<a class="more_arrow" href="' . $fb_link . '" target="_blank"></a>';
            echo "</li>";
        } ?>
    </ul>
    <a class="more" href="https://www.facebook.com/<?php echo $fb_id; ?>" target="_blank">MORE</a>
    <?php
        echo $after_widget;

        $cache[$args['widget_id']] = ob_get_flush();
        wp_cache_set('widget_dnc_facebook', $cache, 'widget');
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['facebook_link'] = strip_tags($new_instance['facebook_link']);
        $instance['max_results'] = strip_tags($new_instance['max_results']);
        $instance['excerpt_length'] = strip_tags($new_instance['excerpt_length']);
        $this->flush_widget_cache();

        $alloptions = wp_cache_get('alloptions', 'options');
        if (isset($alloptions['widget_dnc_facebook'])) {
            delete_option('widget_dnc_facebook');
        }

        return $instance;
    }

    function form($instance) {
        $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
        $facebook_link = isset($instance['facebook_link']) ? esc_attr($instance['facebook_link']) : '';
        $max_results = isset($instance['max_results']) ? esc_attr($instance['max_results']) : '';
        $excerpt_length = isset($instance['excerpt_length']) ? esc_attr($instance['excerpt_length']) : '';
        ?>
    <p>
        <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">Title:</label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
    </p>
    <p>
        <label for="<?php echo esc_attr($this->get_field_id('facebook_link')); ?>">Facebook ID</label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('facebook_link')); ?>" name="<?php echo esc_attr($this->get_field_name('facebook_link')); ?>" type="text" value="<?php echo esc_attr($facebook_link); ?>" />
    </p>
    <p>
        <label for="<?php echo esc_attr($this->get_field_id('max_results')); ?>">Max Results</label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('max_results')); ?>" name="<?php echo esc_attr($this->get_field_name('max_results')); ?>" type="text" value="<?php echo esc_attr($max_results); ?>" />
    </p>
    <p>
        <label for="<?php echo esc_attr($this->get_field_id('excerpt_length')); ?>">Excerpt Length</label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('excerpt_length')); ?>" name="<?php echo esc_attr($this->get_field_name('excerpt_length')); ?>" type="text" value="<?php echo esc_attr($excerpt_length); ?>" />
    </p>
    <?php
    }

    function flush_widget_cache() {
        wp_cache_delete('widget_dnc_facebook', 'widget');
    }
}

function facebook_widget_init() {
    register_widget('DNCSimpleFacebookWidget');
}

add_action('widgets_init', 'facebook_widget_init');

?>